package flow

import (
	"container/heap"
	streams "gitee.com/qw_1215/glink"
	"sync"
	"time"
)

//定义每隔‘silide’时间，统计当前时间过去‘size’时间内的数据的窗口
type SlidingWindow struct {
	sync.Mutex
	size               time.Duration
	slide              time.Duration
	queue              *PriorityQueue
	in                 chan interface{}
	out                chan interface{}
	timestampExtractor func(interface{}) int64
	closed             bool
}

//NewSlidingWindow 返回一个新的滑块处理窗口，
//size - 生成窗口的时间跨度
//slide - 每次滑动的时间
func NewSlidingWindow(size time.Duration, slide time.Duration) *SlidingWindow {
	return NewSlidingWindowWithTsExtractor(size, slide, nil)
}

//生成一个新的时间滑动窗口，可以包容无序事件、延迟事件或者重复出现的事件，并给出正确的过滤结果
//size - 生成窗口的事件跨度
//slide - 每次滑动的时间
//timestampExtractor 时间戳（一纳秒为单位，对应flink时间水印）
func NewSlidingWindowWithTsExtractor(size time.Duration, slide time.Duration,
	timestampExtractor func(interface{}) int64) *SlidingWindow {
	window := &SlidingWindow{
		size:               size,
		slide:              slide,
		queue:              &PriorityQueue{},
		in:                 make(chan interface{}),
		out:                make(chan interface{}), //windows channel
		timestampExtractor: timestampExtractor,
	}
	go window.receive()
	go window.emit()
	return window
}

// 操作指定的数据流
func (sw *SlidingWindow) Via(flow streams.Flow) streams.Flow {
	go sw.transmit(flow)
	return flow
}

// 把指定的数据流输出到接收器
func (sw *SlidingWindow) To(sink streams.Sink) {
	sw.transmit(sink)
}

// 把发送的数据输出到指定输出通道
func (sw *SlidingWindow) Out() <-chan interface{} {
	return sw.out
}

// 把接收到的数据放入接收通道
func (sw *SlidingWindow) In() chan<- interface{} {
	return sw.in
}

// 将需要发送的窗口重新发送的下一个入口流
func (sw *SlidingWindow) transmit(inlet streams.Inlet) {
	for elem := range sw.Out() {
		inlet.In() <- elem
	}
	close(inlet.In())
}

//默认返回系统时间，如果指定了时间戳则从记录中提起时间戳时间
func (sw *SlidingWindow) timestamp(elem interface{}) int64 {
	if sw.timestampExtractor == nil {
		return streams.NowNano()
	}
	return sw.timestampExtractor(elem)
}

//接收消息
func (sw *SlidingWindow) receive() {
	for elem := range sw.in {
		item := &Item{elem, sw.timestamp(elem), 0}
		sw.Lock()
		heap.Push(sw.queue, item)
		sw.Unlock()
	}
	sw.closed = true
	close(sw.out)
}

// 窗口滑动时出发改方法
func (sw *SlidingWindow) emit() {
	for {
		select {
		case <-time.After(sw.slide):
			sw.Lock()
			//创建窗口切片并发送出去
			var slideUpperIndex, windowBottomIndex int
			now := streams.NowNano()
			windowUpperIndex := sw.queue.Len()
			slideUpperTime := now - sw.size.Nanoseconds() + sw.slide.Nanoseconds()
			windowBottomTime := now - sw.size.Nanoseconds()
			for i, item := range *sw.queue {
				if item.epoch < windowBottomTime {
					windowBottomIndex = i
				}
				if item.epoch > slideUpperTime {
					slideUpperIndex = i
					break
				}
			}
			windowSlice := extract(sw.queue.Slice(windowBottomIndex, windowUpperIndex))
			if windowUpperIndex > 0 {
				s := sw.queue.Slice(slideUpperIndex+1, windowUpperIndex)
				//reset queue
				sw.queue = &s
				heap.Init(sw.queue)
			}
			sw.Unlock()
			if sw.closed {
				break
			}
			//send to out chan
			if len(windowSlice) > 0 {
				sw.out <- windowSlice
			}
		}
	}
}

// 生成窗口
func extract(items []*Item) []interface{} {
	rt := make([]interface{}, len(items))
	for i, item := range items {
		rt[i] = item.Msg
	}
	return rt
}
