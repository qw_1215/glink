package flow

import (
	streams "gitee.com/qw_1215/glink"
	"sync"
	"time"
)

// 每隔‘size’时间统计一次过去‘size’时间内的数据，窗口数据不重叠
type TumblingWindow struct {
	sync.Mutex
	size   time.Duration
	in     chan interface{}
	out    chan interface{}
	buffer []interface{}
}

// 返回一个‘TumblingWindow’窗口实例
func NewTumblingWindow(size time.Duration) *TumblingWindow {
	window := &TumblingWindow{
		size: size,
		in:   make(chan interface{}),
		out:  make(chan interface{}), //windows channel
	}
	go window.receive()
	go window.emit()
	return window
}

// 操作指定的数据流
func (tw *TumblingWindow) Via(flow streams.Flow) streams.Flow {
	go tw.transmit(flow)
	return flow
}

// 把指定的数据流输出到接收器
func (tw *TumblingWindow) To(sink streams.Sink) {
	tw.transmit(sink)
}

// 把发送的数据输出到指定输出通道
func (tw *TumblingWindow) Out() <-chan interface{} {
	return tw.out
}

// 把接收到的数据放入接收通道
func (tw *TumblingWindow) In() chan<- interface{} {
	return tw.in
}

//将需要发送的窗口重新发送的下一个入口流
func (tw *TumblingWindow) transmit(inlet streams.Inlet) {
	for elem := range tw.Out() {
		inlet.In() <- elem
	}
	close(inlet.In())
}

//接收消息
func (tw *TumblingWindow) receive() {
	for elem := range tw.in {
		tw.Lock()
		tw.buffer = append(tw.buffer, elem)
		tw.Unlock()
	}
	close(tw.out)
}

// 窗口滑动时出发改方法
func (tw *TumblingWindow) emit() {
	for {
		select {
		case <-time.After(tw.size):
			tw.Lock()
			windowSlice := append(tw.buffer[:0:0], tw.buffer...)
			tw.buffer = nil
			tw.Unlock()
			//send to out chan
			if len(windowSlice) > 0 {
				tw.out <- windowSlice
			}
		}
	}
}
