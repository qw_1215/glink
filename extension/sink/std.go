package sink

import "fmt"

// IgnoreSink sends items to /dev/null
type StdoutSink struct {
	in chan interface{}
}

// NewIgnoreSink returns new IgnoreSink instance
func NewStdoutSink() *StdoutSink {
	sink := &StdoutSink{make(chan interface{})}
	sink.init()
	return sink
}

func (std *StdoutSink) init() {
	go func() {
		for {
			msg, ok := <-std.in
			if !ok {
				break
			}
			fmt.Println("----:",msg)
		}
	}()
}

// In returns channel for receiving data
func (ignore *StdoutSink) In() chan<- interface{} {
	return ignore.in
}
