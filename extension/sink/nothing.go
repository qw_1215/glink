package sink

// IgnoreSink sends items to /dev/null
type NothingSink struct {
	in chan interface{}
}

// NewIgnoreSink returns new IgnoreSink instance
func NewNothingSink() *NothingSink {
	sink := &NothingSink{make(chan interface{})}
	sink.init()
	return sink
}

func (n *NothingSink) init() {
	go func() {
		for {
			_, ok := <-n.in
			if !ok {
				break
			}
		}
	}()
}

// In returns channel for receiving data
func (n *NothingSink) In() chan<- interface{} {
	return n.in
}

