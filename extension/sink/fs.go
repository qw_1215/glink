package sink

import (
	streams "gitee.com/qw_1215/glink"
	"os"
)

// FileSink stores items to file
type FileSink struct {
	fileName string
	in       chan interface{}
}

// NewFileSink returns new FileSink instance
func NewFileSink(fileName string) *FileSink {
	sink := &FileSink{fileName, make(chan interface{})}
	sink.init()
	return sink
}

func (fs *FileSink) init() {
	go func() {
		file, err := os.OpenFile(fs.fileName, os.O_CREATE|os.O_WRONLY, 0600)
		streams.Check(err)
		defer file.Close()
		for elem := range fs.in {
			_, err = file.WriteString(elem.(string))
			streams.Check(err)
		}
	}()
}

// In returns channel for receiving data
func (fs *FileSink) In() chan<- interface{} {
	return fs.in
}
