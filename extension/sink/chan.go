package sink

// ChanSink transmitts data to chan
type ChanSink struct {
	Out chan interface{}
}

// NewChanSink returns new ChanSink instance
func NewChanSink(out chan interface{}) *ChanSink {
	return &ChanSink{out}
}

// In returns channel for receiving data
func (ch *ChanSink) In() chan<- interface{} {
	return ch.Out
}
