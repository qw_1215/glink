package sink

import (
	"context"
	streams "gitee.com/qw_1215/glink"
	"github.com/apache/pulsar-client-go/pulsar"
	"log"
)

// PulsarSink connector
type PulsarSink struct {
	client   pulsar.Client
	producer pulsar.Producer
	in       chan interface{}
}

// NewPulsarSink creates a new PulsarSink
func NewPulsarSink(clientOptions *pulsar.ClientOptions, producerOptions *pulsar.ProducerOptions) *PulsarSink {
	client, err := pulsar.NewClient(*clientOptions)
	streams.Check(err)
	producer, err := client.CreateProducer(*producerOptions)
	streams.Check(err)
	sink := &PulsarSink{
		client:   client,
		producer: producer,
		in:       make(chan interface{}),
	}
	go sink.init()
	return sink
}

// start main loop
func (ps *PulsarSink) init() {
	ctx := context.Background()
	for msg := range ps.in {
		switch m := msg.(type) {
		case pulsar.Message:
			_, _ = ps.producer.Send(ctx, &pulsar.ProducerMessage{
				Payload: m.Payload(),
			})
		case string:
			_, _ = ps.producer.Send(ctx, &pulsar.ProducerMessage{
				Payload: []byte(m),
			})
		}
	}
	log.Printf("Closing pulsar producer")
	ps.producer.Close()
	ps.client.Close()
}

// In returns channel for receiving data
func (ps *PulsarSink) In() chan<- interface{} {
	return ps.in
}
