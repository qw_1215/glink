package sink

import (
	"bufio"
	"log"
	"net"
	"time"
)

// ConnType connection type
type ConnType string


// NetSink downstreams input events to a network soket
type NetSink struct {
	conn     net.Conn
	connType ConnType
	in       chan interface{}
}

// NewNetSink creates a new NetSink
func NewNetSink(connType ConnType, address string) (*NetSink, error) {
	var err error
	var conn net.Conn

	conn, err = net.DialTimeout(string(connType), address, time.Second*10)
	if err != nil {
		return nil, err
	}

	sink := &NetSink{
		conn:     conn,
		connType: connType,
		in:       make(chan interface{}),
	}

	go sink.init()
	return sink, nil
}

// start main loop
func (ns *NetSink) init() {
	log.Printf("NetSink connected on: %v", ns.conn.LocalAddr())
	writer := bufio.NewWriter(ns.conn)
	for msg := range ns.in {
		switch m := msg.(type) {
		case string:
			_, err := writer.WriteString(m)
			if err == nil {
				err = writer.Flush()
			}
		}
	}
	log.Printf("Closing NetSink connection %v", ns.conn)
	ns.conn.Close()
}

// In returns channel for receiving data
func (ns *NetSink) In() chan<- interface{} {
	return ns.in
}
