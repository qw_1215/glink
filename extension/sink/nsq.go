package sink

import (
	"fmt"
	"github.com/gogf/gf/encoding/gjson"
	"github.com/nsqio/go-nsq"
	"regexp"
	"strings"
)

type NsqSink struct {
	producer *nsq.Producer
	in      chan interface{}
}

func NewNsqSink(url string, conf *nsq.Config) (*NsqSink, error) {
	if conf == nil {
		conf = nsq.NewConfig()
	}
	producer, err := nsq.NewProducer(url, conf)
	if err != nil {
		return nil,err
	}
	ns := &NsqSink{
		producer: producer,
		in:    make(chan interface{}),
	}
	go ns.init()
	return ns,nil
}

func (ns *NsqSink) init() error {

	for msg := range ns.in {
		switch m := msg.(type) {
		case nsq.Message:
			j, err := gjson.DecodeToJson(m.Body)
			if err != nil {
				fmt.Println(err)
				continue
			}
			topic := regexp.MustCompile(`/\w*/\w*/`).FindString(j.GetString("topic"))
			if len(topic) >0 {
				topic = topic[1 : len(topic)-1]
				topic = strings.ReplaceAll(topic, "/", "-")
			}
			if err := ns.producer.Publish(topic, m.Body); err != nil{
				fmt.Println(err)
			}
		}
	}
	return nil
}

// In returns channel for receiving data
func (ns *NsqSink) In() chan<- interface{} {
	return ns.in
}