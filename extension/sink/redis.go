package sink

import (
	"github.com/go-redis/redis"
	"log"
)

// RedisSink implements Redis Pub/Sub Sink
type RedisSink struct {
	redisdb *redis.Client
	channel string
	in      chan interface{}
}

// NewRedisSink returns new RedisSink instance
func NewRedisSink(config *redis.Options, channel string) *RedisSink {
	sink := &RedisSink{
		redis.NewClient(config),
		channel,
		make(chan interface{}),
	}
	go sink.init()
	return sink
}

//start main loop
func (rs *RedisSink) init() {
	for msg := range rs.in {
		switch m := msg.(type) {
		case string:
			log.Println("Redis message:", m)
			err := rs.redisdb.Publish(rs.channel, m).Err()
			if err != nil {
				panic(err)
			}
		}
	}
}

// In returns channel for receiving data
func (rs *RedisSink) In() chan<- interface{} {
	return rs.in
}
