package source

import (
	streams "gitee.com/qw_1215/glink"
	"gitee.com/qw_1215/glink/flow"
	"github.com/nats-io/nats.go"
)

type NatsSource struct {
	conn *nats.Conn
	out  chan interface{}
}

func NewNatsSource(addr string, topic string) (*NatsSource, error) {
	// Connect to a server
	nc, err := nats.Connect(addr)
	streams.Check(err)
	source := &NatsSource{
		conn: nc,
		out:  make(chan interface{}),
	}
	go source.init(topic)
	return source, nil
}

func (ns *NatsSource) init(topic string) {
	ec, _ := nats.NewEncodedConn(ns.conn, nats.JSON_ENCODER)
	defer ec.Close()
	for {
		_, e := ec.BindRecvChan(topic, ns.out)
		if e != nil {
			panic(e)
		}
	}

	//ch := make(chan *nats.Msg,64)
	//_,err := ns.conn.ChanSubscribe(topic,ch)
	//if err != nil {
	//	log.Panicf("定于主题<%s>出错：%s\n",topic,err)
	//}
	//for {
	//	select {
	//	case msg := <-ch:
	//		ns.out <-msg.Data
	//	}
	//}

}

// Via streams data through given flow
func (ns *NatsSource) Via(_flow *flow.Map) streams.Flow {
	flow.DoStream(ns, _flow)
	return _flow
}

// Out returns channel for sending data
func (ns *NatsSource) Out() <-chan interface{} {
	return ns.out
}
