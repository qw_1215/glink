package source

import (
	streams "gitee.com/qw_1215/glink"
	"gitee.com/qw_1215/glink/flow"
)

// ChanSource streams data from chan
type ChanSource struct {
	in chan interface{}
}

// NewChanSource returns new ChanSource instance
func NewChanSource(in chan interface{}) *ChanSource {
	return &ChanSource{in}
}

// Via streams data through given flow
func (cs *ChanSource) Via(_flow streams.Flow) streams.Flow {
	flow.DoStream(cs, _flow)
	return _flow
}

// Out returns channel for sending data
func (cs *ChanSource) Out() <-chan interface{} {
	return cs.in
}
