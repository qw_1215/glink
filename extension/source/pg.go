package source

import (
	"database/sql"
	streams "gitee.com/qw_1215/glink"
	"gitee.com/qw_1215/glink/flow"
	_ "github.com/lib/pq"
)

type PgSource struct {
	db *sql.DB
	out  chan interface{}
}

func NewPgSource(connStr string) (*PgSource,error) {
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		return nil,err
	}
	pg := &PgSource{
		db: db,
		out:  make(chan interface{}),
	}
	return pg,nil
}

func (p *PgSource) init ()  {

}

// Via streams data through given flow
func (p *PgSource) Via(_flow *flow.Map) streams.Flow {
	flow.DoStream(p, _flow)
	return _flow
}

// Out returns channel for sending data
func (p *PgSource) Out() <-chan interface{} {
	return p.out
}
