package source

import (
	"context"
	"github.com/apache/pulsar-client-go/pulsar"
	streams "gitee.com/qw_1215/glink"
	"gitee.com/qw_1215/glink/flow"
	"log"
)

// PulsarSource connector
type PulsarSource struct {
	client   pulsar.Client
	consumer pulsar.Consumer
	out      chan interface{}
}

// NewPulsarSource creates a new PulsarSource
func NewPulsarSource(clientOptions *pulsar.ClientOptions, consumerOptions *pulsar.ConsumerOptions) (*PulsarSource, error) {
	client, err := pulsar.NewClient(*clientOptions)
	streams.Check(err)
	consumer, err := client.Subscribe(*consumerOptions)
	streams.Check(err)
	source := &PulsarSource{
		client:   client,
		consumer: consumer,
		out:      make(chan interface{}),
	}
	go source.init()
	return source, nil
}

// start main loop
func (ps *PulsarSource) init() {
	for {
		msg, err := ps.consumer.Receive(context.Background())
		if err == nil {
			ps.out <- msg
		} else {
			log.Fatal(err)
		}
	}
	//by will 无法执行的代码呀
	log.Printf("Closing pulsar 订阅")
	ps.consumer.Close()
	ps.client.Close()
}

// Via streams data through given flow
func (ps *PulsarSource) Via(_flow *flow.Map) streams.Flow {
	flow.DoStream(ps, _flow)
	return _flow
}

// Out returns channel for sending data
func (ps *PulsarSource) Out() <-chan interface{} {
	return ps.out
}
