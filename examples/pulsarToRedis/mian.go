package main

import (
	"github.com/apache/pulsar-client-go/pulsar"
	streams "gitee.com/qw_1215/glink"
	si "gitee.com/qw_1215/glink/extension/sink"
	so "gitee.com/qw_1215/glink/extension/source"
	"gitee.com/qw_1215/glink/flow"
	"strings"
	"time"
)

func main() {
	clientOptions := pulsar.ClientOptions{URL: "pulsar://172.17.126.201:6650"}
	consumerOptions := pulsar.ConsumerOptions{
		Topic:            "test1",
		SubscriptionName: "group1",
		Type:             pulsar.Exclusive,
	}

	source, err := so.NewPulsarSource(&clientOptions, &consumerOptions)
	streams.Check(err)
	flow1 := flow.NewMap(toUpper, 1)
	flow2 := flow.NewMap(toSum, 1)
	sink := si.NewStdoutSink()

	slidingWindow := flow.NewSlidingWindow(time.Second*10, time.Second*6)
	//tumblingWindow := flow.NewTumblingWindow(time.Second * 1)

	source.Via(flow1).Via(slidingWindow).Via(flow2).To(sink)
}

var (
	toUpper = func(in interface{}) interface{} {
		msg := in.(pulsar.Message)
		mm := strings.ToUpper(string(msg.Payload()))
		return mm
	}
	toSum = func(in interface{}) interface{} {
		arr := in.([]interface{})
		rt := make([]string, len(arr))
		for i, item := range arr {
			msg := item.(string)
			rt[i] = msg + "*"
		}
		return strings.Join(rt, ",")
	}
)
