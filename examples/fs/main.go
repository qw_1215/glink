package main

import (
	si "gitee.com/qw_1215/glink/extension/sink"
	so "gitee.com/qw_1215/glink/extension/source"
	"gitee.com/qw_1215/glink/flow"
	"time"
)

func main() {
	source := so.NewFileSource("E:\\dev\\go\\github\\neptune\\glink\\examples\\fs\\in.txt")
	flow := flow.NewMap(reverse, 1)
	sink := si.NewFileSink("out.txt")

	source.Via(flow).To(sink)

	time.Sleep(time.Second)
}

var reverse = func(in interface{}) interface{} {
	s := in.(string)
	var reverse string
	for i := len(s) - 1; i >= 0; i-- {
		reverse += string(s[i])
	}
	return reverse
}
