package main

import (
	streams "gitee.com/qw_1215/glink"
	si "gitee.com/qw_1215/glink/extension/sink"
	so "gitee.com/qw_1215/glink/extension/source"
	"gitee.com/qw_1215/glink/flow"
	"strings"

	"github.com/go-redis/redis"
)

//docker exec -it pubsub bash
//https://redis.io/topics/pubsub
func main() {
	config := &redis.Options{
		Addr:     "localhost:6379", // use default Addr
		Password: "",               // no password set
		DB:       0,                // use default DB
	}
	source, err := so.NewRedisSource(config, "api")
	streams.Check(err)
	flow1 := flow.NewMap(toUpper, 1)
	sink := si.NewRedisSink(config, "test2")

	source.Via(flow1).To(sink)
}

var toUpper = func(in interface{}) interface{} {
	msg := in.(*redis.Message)
	return strings.ToUpper(msg.Payload)
}
