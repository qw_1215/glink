module gitee.com/qw_1215/glink

go 1.14

require (

	github.com/eclipse/paho.mqtt.golang v1.2.0
	github.com/go-redis/redis v6.15.7+incompatible
	github.com/gogf/gf v1.11.7
	github.com/lib/pq v1.3.0
	github.com/nats-io/nats-server/v2 v2.1.4 // indirect
	github.com/nats-io/nats.go v1.9.1
	github.com/nsqio/go-nsq v1.0.8
	github.com/onsi/ginkgo v1.12.0 // indirect
	github.com/onsi/gomega v1.9.0 // indirect
	golang.org/x/net v0.0.0-20200301022130-244492dfa37a // indirect
)
